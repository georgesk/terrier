!contains(DEFINES, .*__ABULEDU_MULTIMEDIA_AVAILABLE__.*) {
  DEFINES += __ABULEDU_MULTIMEDIA_AVAILABLE__=1
}

 QT += multimedia multimediawidgets



INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
        $$PWD/abuledumultimediav1.h\
        $$PWD/abuledumediaviewerinterface.h \
        $$PWD/abuleduslidesviewerv1.h \
        $$PWD/abuleducontrolaudiov1.h \
        $$PWD/abuleduvideoviewerv1.h\
        $$PWD/abuledusoundplayerv1.h \
        $$PWD/abuleduanimatedgifviewerv1.h \
        $$PWD/abuledumediabuttonv1.h \
        $$PWD/abuledusoundrecorderv1.h \
        $$PWD/abuleduaudiolevelwidgetv1.h \
        $$PWD/abuledumediagraphicsbuttonv1.h \
        $$PWD/abuledupictureplayerv1.h \
        $$PWD/abuleduaudiodecoderv1.h \
        $$PWD/abuledusounddevices.h \
        $$PWD/abuleduslidesrecorderv1.h \
        $$PWD/abuleduaudioplayerv1.h \
        $$PWD/abuledumultimediawidgetcontainer.h \
        $$PWD/abuledumultimediasettingsv1.h \
        $$PWD/abuledumultimediadevicechooserv1.h \
        $$PWD/abuledumediamedias.h

SOURCES += \
        $$PWD/abuledumultimediav1.cpp\
        $$PWD/abuleducontrolaudiov1.cpp \
        $$PWD/abuleduslidesviewerv1.cpp \
        $$PWD/abuleduvideoviewerv1.cpp \
        $$PWD/abuledusoundplayerv1.cpp \
        $$PWD/abuleduanimatedgifviewerv1.cpp \
        $$PWD/abuledumediabuttonv1.cpp \
        $$PWD/abuledusoundrecorderv1.cpp \
        $$PWD/abuleduaudiolevelwidgetv1.cpp \
        $$PWD/abuledumediagraphicsbuttonv1.cpp \
        $$PWD/abuledupictureplayerv1.cpp \
        $$PWD/abuleduaudiodecoderv1.cpp \
        $$PWD/abuleduslidesrecorderv1.cpp \
        $$PWD/abuleduaudioplayerv1.cpp \
        $$PWD/abuledumultimediawidgetcontainer.cpp \
        $$PWD/abuledumultimediasettingsv1.cpp \
        $$PWD/abuledumultimediadevicechooserv1.cpp \
        $$PWD/abuledumediamedias.cpp


#RESOURCES += \
#        $$PWD/abuleducontrolaudiov1.qrc

FORMS += \
        $$PWD/abuledumultimediav1.ui \
        $$PWD/abuleducontrolaudiov1.ui \
        $$PWD/abuledumultimediadevicechooserv1.ui

RESOURCES += $$PWD/abuleducontrolaudiov1.qrc
