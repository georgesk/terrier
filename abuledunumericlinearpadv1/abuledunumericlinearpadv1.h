/** Pavé numérique linéaire composé d'AbulEduFlatBoutonV1
  * Créé dans le logiciel Contour par Jean-Louis Frucot, la nature des signaux envoyés
  * est passée de QString en Qt::Key au moment de sa transformation en librairie externe
  *
  * Utilisation: https://redmine.ryxeo.com/projects/ryxeo/wiki/AbulEduNumericLinearPadv1
  *
  * @warning aucun traitement d'erreur n'est pour l'instant implémenté
  * @see https://redmine.ryxeo.com/projects/leterrier-imagessequentielles
  * @author 2013 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @author 2015 Philippe Cadaugade <philippe.cadaugade@ryxeo.com>
  * @see The GNU Public License (GPL)
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program; if not, write to the Free Software Foundation, Inc.,
  * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */

#ifndef ABULEDUNUMERICLINEARPADV1_H
#define ABULEDUNUMERICLINEARPADV1_H

#include <QWidget>
#include <QDebug>
#include <QSettings>
#include <QSignalMapper>

#include "abuleduflatboutonv1.h"

#ifndef DEBUG_NUMERICLINEARPADV1
#define  DEBUG_NUMERICLINEARPADV1 0
#endif
namespace Ui {
class AbulEduNumericLinearPadv1;
}

class AbulEduNumericLinearPadv1 : public QWidget
{
    Q_OBJECT
    
public:
    ///
    /// \brief Instancie un AbulEduNumericLinearPadv1
    /// \param height la hauteur souhaitée
    /// \param asKeyboard la propriété d'imiter le clavier (0 après 9)
    /// \param parent le parent
    ///
    explicit AbulEduNumericLinearPadv1(float height = 60, bool asKeyboard = false, QWidget *parent = 0);
    ~AbulEduNumericLinearPadv1();

    ///
    /// \brief Fixe la hauteur du pad
    /// \param height la hauteur souhaitée
    ///
    void setFixedHeight(float height);
private:
    ///
    /// \brief Interface graphique de l'AbulEduNumericLinearPadv1
    ///
    Ui::AbulEduNumericLinearPadv1 *ui;

    ///
    /// \brief Crée des correspondances entre les boutons et les signaux
    ///
    QSignalMapper *signalMapper;

    ///
    /// \brief Hauteur de l'AbulEduNumericLinearPadv1
    ///
    float m_height;

    ///
    /// \brief Contrôle l'affichage des debug
    ///
    bool m_localDebug;

    ///
    /// \brief Dessine le bouton du zéro
    /// \note Le dessin de ce bouton est séparé pour pouvoir, selon la valeur du paramètre du constructeur "asKeyboard", le placer avant le 1 ou après le 9
    ///
    void abeNumericLinearPadSetZero();

private slots:
    ///
    /// \brief slotNumericLinearPadEmitKey
    /// \param key est l'int reçu au clic sur le bouton, que le slot transforme en Qt::Key pour le réémettre
    ///
    void slotNumericLinearPadEmitKey(int key);
signals:
    ///
    /// \brief Signal émis au clic sur une touche avec la Qt::Key qui correspond au pavé numérique
    ///
    void signalNumericLinearPadV1Clicked(Qt::Key);
};

#endif // ABULEDUNUMERICLINEARPADV1_H
