/** Pavé numérique linéaire composé d'AbulEduFlatBoutonV1
  * Créé dans le logiciel Contour par Jean-Louis Frucot, la nature des signaux envoyés
  * est passée de QString en Qt::Key au moment de sa transformation en librairie externe
  *
  * Utilisation: https://redmine.ryxeo.com/projects/ryxeo/wiki/AbulEduNumericLinearPadv1
  *
  * @warning aucun traitement d'erreur n'est pour l'instant implémenté
  * @see https://redmine.ryxeo.com/projects/leterrier-imagessequentielles
  * @author 2013 Jean-Louis Frucot <frucot.jeanlouis@free.fr>
  * @author 2015 Philippe Cadaugade <philippe.cadaugade@ryxeo.com>
  * @see The GNU Public License (GPL)
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program; if not, write to the Free Software Foundation, Inc.,
  * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  */

#include "abuledunumericlinearpadv1.h"
#include "ui_abuledunumericlinearpadv1.h"
#include "abuleduapplicationv1.h"

AbulEduNumericLinearPadv1::AbulEduNumericLinearPadv1(float height, bool asKeyboard, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AbulEduNumericLinearPadv1),
    signalMapper(new QSignalMapper(this)),
    m_height(height),
    m_localDebug(DEBUG_NUMERICLINEARPADV1)
{
    if(m_localDebug){
        qDebug()  << __PRETTY_FUNCTION__;
    }

    ui->setupUi(this);

    /* Création des touches */
    AbulEduFlatBoutonV1 *button = new AbulEduFlatBoutonV1(this);
    button->setObjectName("abeNumericLinearPadBtnMinus");
    button->setIconeNormale(":/keys/key_minus");
    button->setIconeSurvol(":/keys/key_minus_hover");
    button->setIconePressed(":/keys/key_minus_hover");
    button->setFixedSize(m_height, m_height);
    ui->hl_keys->addWidget(button);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(button, Qt::Key_Minus);

    if(!asKeyboard){
        abeNumericLinearPadSetZero();
    }

    for(int i=1; i < 10; i++)
    {
        int tr = i+48;
        Qt::Key touche = (Qt::Key) tr;
        AbulEduFlatBoutonV1 *button = new AbulEduFlatBoutonV1(this);
        button->setObjectName("abeNumericLinearPadBtn"+QString::number(i));
        button->setIconeNormale(":/keys/key_"+QString::number(i));
        button->setIconeSurvol(":/keys/key_"+QString::number(i)+"_hover");
        button->setIconePressed(":/keys/key_"+QString::number(i)+"_hover");
        ui->hl_keys->addWidget(button);
        button->setFixedSize(m_height, m_height);
        connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(button, touche);
    }

    if(asKeyboard){
        abeNumericLinearPadSetZero();
    }

    button = new AbulEduFlatBoutonV1(this);
    button->setObjectName("abeNumericLinearPadBtnBackspace");
    button->setIconeNormale(":/keys/key_backspace");
    button->setIconeSurvol(":/keys/key_backspace_hover");
    button->setIconePressed(":/keys/key_backspace_hover");
    ui->hl_keys->addWidget(button);
    button->setFixedSize(m_height, m_height);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(button, Qt::Key_Backspace);

    button = new AbulEduFlatBoutonV1(this);
    button->setObjectName("abeNumericLinearPadBtnEnter");
    button->setIconeNormale(":/keys/key_enter");
    button->setIconeSurvol(":/keys/key_enter_hover");
    button->setIconePressed(":/keys/key_enter_hover");
    ui->hl_keys->addWidget(button);
    button->setFixedSize(m_height, m_height);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(button, Qt::Key_Enter);

    connect(signalMapper, SIGNAL(mapped(int)),
            this, SLOT(slotNumericLinearPadEmitKey(int)),Qt::UniqueConnection);

    /* Lecture du fichier conf pour attribuer les btn */
    QList<AbulEduFlatBoutonV1 *> btns = findChildren<AbulEduFlatBoutonV1 *>();
    QSettings config(":/abuledunumericlinearpadv1/abuledunumericlinearpadv1.conf",QSettings::IniFormat, this);
    qDebug()<<"&&"<<config.childGroups();
    foreach(const QString &gr, config.childGroups()) {
        config.beginGroup(gr);
        {
            /* Sous android c'est un menu intégré à base de QActions */
            //#ifdef Q_OS_ANDROID
            //        for(int i = 0; i < actions.count(); i++) {
            //            if(actions.at(i)->objectName() == ("action" + gr)) {
            //                QAction *btn = actions.takeAt(i);
            //                if(m_localDebug) qDebug() << " on a trouvé " << btn->objectName();

            //                foreach(QString propertie, grProperties) {
            //                    if(m_localDebug) qDebug() << " propriété -> " << propertie.toLatin1()<<" contient "<<config.value(propertie);
            //                    /* Dans le cas de la propriété text seulement, on gère l'utf8 */
            //                    if(propertie.toLatin1() == "text")
            //                    {
            //                        QByteArray str = config.value(propertie).toByteArray();
            //                        btn->setProperty("text", QString::fromUtf8(str));
            //                    }
            //                    else
            //                    {
            //                        btn->setProperty(propertie.toLatin1(), config.value(propertie));
            //                    }
            //                }
            //            }
            //        }
            //#else

        }
        /* Sur les autres OS, on cherche le widget qui porte ce nom dans notre liste de pointeurs de qwidgets */
        /* 1. Foreach sur tous les widgets */
        foreach (AbulEduFlatBoutonV1 *b, btns) {
            /* 2. Si le widget == à gr */
            if(b->objectName() == gr){
                /* 3. On définit les propriétés */
                foreach (const QString &propertie, config.childKeys()) {
                    b->setProperty(propertie.toUtf8(), config.value(propertie));
                }
                b->style()->unpolish(b);
                b->style()->polish(b);
                b->update();
            }
        }
        config.endGroup();
    }
}

AbulEduNumericLinearPadv1::~AbulEduNumericLinearPadv1()
{
    if(m_localDebug){
        qDebug()  << __PRETTY_FUNCTION__;
    }
    /* On détruit tous les boutons créés dans le constructeur */
    QLayoutItem *child;
    while ((child = ui->hl_keys->takeAt(0)) != 0)
    {
        delete child;
    }
    delete ui;
}

void AbulEduNumericLinearPadv1::setFixedHeight(float height)
{
    if(m_localDebug){
        qDebug()  << __PRETTY_FUNCTION__<<height;
    }
    for (int i = 0; i < ui->hl_keys->count(); ++i)
    {
        AbulEduFlatBoutonV1 *button = static_cast<AbulEduFlatBoutonV1 *>(ui->hl_keys->itemAt(i)->widget());
        if(button)
        {
            button->setFixedSize(height, height);
        }
    }
    m_height = height;
}

void AbulEduNumericLinearPadv1::abeNumericLinearPadSetZero()
{
    if(m_localDebug){
        qDebug()  << __PRETTY_FUNCTION__;
    }
    AbulEduFlatBoutonV1 *button = new AbulEduFlatBoutonV1(this);
    button->setObjectName("abeNumericLinearPadBtn0");
    button->setIconeNormale(":/keys/key_0");
    button->setIconeSurvol(":/keys/key_0_hover");
    button->setIconePressed(":/keys/key_0_hover");
    ui->hl_keys->addWidget(button);
    button->setFixedSize(m_height, m_height);
    connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(button, Qt::Key_0);
}

void AbulEduNumericLinearPadv1::slotNumericLinearPadEmitKey(int key)
{
    if(m_localDebug){
        qDebug()  << __PRETTY_FUNCTION__<<key;
    }
    /* Astuce : X-48 donne la valeur du nombre envoyé par Qt::Key_X */
    emit signalNumericLinearPadV1Clicked((Qt::Key) key);
}
