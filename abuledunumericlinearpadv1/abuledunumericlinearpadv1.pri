 INCLUDEPATH += $$PWD
 DEPENDPATH += $$PWD

HEADERS += $$PWD/abuledunumericlinearpadv1.h
 
 SOURCES += $$PWD/abuledunumericlinearpadv1.cpp
 
 FORMS += $$PWD/abuledunumericlinearpadv1.ui

 
# si fichier ressource, on inclut le fichier ressources du developpeur de
# l'application en prioritemais seulement s'il existe. S'il n'existe pas on
# fallback sur le fichier de ressources propose par la lib
 !exists($$PWD/../../data/abuledunumericlinearpadv1/abuledunumericlinearpadv1.qrc) {
  RESOURCES += $$PWD/abuledunumericlinearpadv1.qrc
 }
 exists($$PWD/../../data/abuledunumericlinearpadv1/abuledunumericlinearpadv1.qrc) {
   RESOURCES += $$PWD/../../data/abuledunumericlinearpadv1/abuledunumericlinearpadv1.qrc
}

!exists($$PWD/../../data/abuledunumericlinearpadv1/abuledunumericlinearpadv1.conf) {
  OTHER_FILES = $$PWD/abuledunumericlinearpadv1.conf
} else {
  OTHER_FILES = $$PWD/../../data/abuledunumericlinearpadv1/abuledunumericlinearpadv1.conf
}
