TEMPLATE = lib

DESTDIR = "build"
RCC_DIR =  "build"
UI_DIR =  "build"
MOC_DIR =  "build"
OBJECTS_DIR = "build"


include(abuleduconstantesv1/abuleduconstantesv1.pri)
include(abuleduloggerv1/abuleduloggerv1.pri)
include(maia/maia.pri)
include(abuledunetworkaccessmanagerv1/abuledunetworkaccessmanagerv1.pri)
include(abuleduapplicationv1/abuleduapplicationv1.pri)
## include(abuleduapplicationv1/debug.pri)
## include(abuleduapplicationv1/qtsingleapplication/common.pri)
## include(abuleduapplicationv1/qtsingleapplication/src/qtsingleapplication.pri)
## include(abuleduapplicationv1/qtsingleapplication/src/qtsinglecoreapplication.pri)
include(abuleduidentitesv1/abuleduidentitesv1.pri)
include(abuleduflatboutonv1/abuleduflatboutonv1.pri)
include(abuledumessageboxv1/abuledumessageboxv1.pri)
include(abuledupluginloaderv1/abuledupluginloaderv1.pri)
include(abuledusingletonv1/abuledusingletonv1.pri)
include(abuledumultimediav1/abuledumultimediav1.pri)
include(abuledupicottsv1/abuledupicottsv1.pri)
include(abuleduaudiov1/abuleduaudiov1.pri)
include(abuleduexercicev1/abuleduexercicev1.pri)
include(abuledupageaccueilv1/abuledupageaccueilv1.pri)
include(abuledugraphicmenuv1/abuledugraphicmenuv1.pri)
include(abuledulabelv1/abuledulabelv1.pri)
include(abuledufilev1/abuledufilev1.pri)
## include(abuledufilev1/quazip/includes.pri)
## DÉJÀ INCLUS PAR abuledufilev1.pri: include(abuledufilev1/quazip/quazip/quazip.pri)
## DÉJÀ INCLUS PAR abuledufilev1.pri: include(abuledufilev1/vcard/vcard.pri)
include(abuleduloadinganimationv1/abuleduloadinganimationv1.pri)
include(abuledutetev1/abuledutetev1.pri)
include(abuledusplashscreenv1/abuledusplashscreenv1.pri)
include(abuleducommonstatesv1/abuleducommonstatesv1.pri)
include(abuledustatemachinev1/abuledustatemachinev1.pri)
include(abuleduetiquettesv1/abuleduetiquettesv1.pri)
include(abuleduaproposv1/abuleduaproposv1.pri)
include(abuledustackedwidgetv1/abuledustackedwidgetv1.pri)
include(abuledunumericlinearpadv1/abuledunumericlinearpadv1.pri)
## NE MARCHE PAS BIEN ! include(abuledulangv1/abuledulangv1.pri)
