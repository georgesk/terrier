# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/libterrier1/issues
# Bug-Submit: https://github.com/<user>/libterrier1/issues/new
# Changelog: https://github.com/<user>/libterrier1/blob/master/CHANGES
# Documentation: https://github.com/<user>/libterrier1/wiki
# Repository-Browse: https://github.com/<user>/libterrier1
# Repository: https://github.com/<user>/libterrier1.git
