QT += multimedia multimediawidgets


INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD/abuledupicottsv1.h
SOURCES += $$PWD/abuledupicottsv1.cpp


linux-g++-32|linux-g++-64|linux-g++ {
  ! exists( /usr/include/picoapi.h ) {
    error("LibTTS manquante ! assurez vous d'avoir les paquets suivants d'installés: libttspico-dev libttspico0 libttspico-data")
  }
  LIBS     += -L/usr/lib/ -lttspico
  INCLUDEPATH += /usr/include
}


android {
  exists(/opt/android/aosp-4.0/out/target/product/generic/system/lib/libttspico.so) {
    LIBS     += -L/opt/android/aosp-4.0/out/target/product/generic/system/lib/ -lttspico
    INCLUDEPATH +=
    ANDROID_EXTRA_LIBS += /opt/android/aosp-4.0/out/target/product/generic/system/lib/libttspico.so
  }
  exists(/opt/android/svox/libs/armeabi/libttspico.so) {
    LIBS     += -L/opt/android/svox/libs/armeabi/ -lttspico
    INCLUDEPATH +=
    ANDROID_EXTRA_LIBS += /opt/android/svox/libs/armeabi/libttspico.so
    ANDROID_EXTRA_LIBS += /opt/android/svox/libs/armeabi/libttscompat.so
  }
  #!exists(/opt/android/aosp-4.0/out/target/product/generic/system/lib/libttspico.so) {
  #  LIBS     += -L/opt/android/svox/obj/local/armeabi -lsvoxpico
  #  INCLUDEPATH += /opt/android/svox/pico
  #}
}
