QT       += core

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

DEFINES += ABULEDU_LOG_LINE_NUMBERS ABULEDU_LOG_SEPARATE_THREAD

SOURCES +=  $$PWD/abuleduloggerv1.cpp \
            $$PWD/abuledulogdestinationv1.cpp \
            $$PWD/abuledulogdestinationconsolev1.cpp \
            $$PWD/abuledudestinationfilev1.cpp

HEADERS +=  $$PWD/abuleduloggerv1.h\
            $$PWD/abuleduloglevelv1.h \
            $$PWD/abuledulogdestinationv1.h \
            $$PWD/abuledulogdestinationconsolev1.h \
            $$PWD/abuledudestinationfilev1.h \
            $$PWD/abuledudisableloggerv1.h
