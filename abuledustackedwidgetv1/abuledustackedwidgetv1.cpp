/**
  * @class AbulEduStackedWidgetV1
  * @file  abuledustackedwidgetv1.cpp
  *
  * @author 2013-2014 Philippe Cadaugade <philippe.cadaugade@ryxeo.com>
  * @author Icham Sirat <icham.sirat@ryxeo.com>
  * @date 2013 09 28
  * @see https://redmine.ryxeo.com/projects/ryxeo/wiki/AbulEduPluginLoaderV1
  * @see The GNU Public License (GPL)
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program; if not, write to the Free Software Foundation, Inc.,
  * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  *
  * This file is part of The AbulEdu Project
  */
#include "abuledustackedwidgetv1.h"

#ifndef DEBUG_ABULEDUSTACKEDWIDGETV1
     #include "abuledudisableloggerv1.h"
#endif

AbulEduStackedWidgetV1::AbulEduStackedWidgetV1(QWidget *parent) :
    QStackedWidget(parent)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    m_mainwindow=parent;
    m_vertical=false;
    //abeStackedWidgetSetVerticalMode(true);
    m_speed=1000;
    m_animationtype = QEasingCurve::OutBack;
    m_now=0;
    m_next=0;
    m_wrap=false;
    m_pnow=QPoint(0,0);
    m_active=false;
}

AbulEduStackedWidgetV1::~AbulEduStackedWidgetV1()
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    /* Pas de pointeur à nettoyer, pas de ui, donc rien à faire */
}

void AbulEduStackedWidgetV1::abeStackedWidgetSetVerticalMode(bool vertical)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    m_vertical=vertical;
}

void AbulEduStackedWidgetV1::abeStackedWidgetSetSpeed(int speed)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    m_speed = speed;
}

void AbulEduStackedWidgetV1::abeStackedWidgetSetAnimation(enum QEasingCurve::Type animationtype)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    m_animationtype = animationtype;
}

void AbulEduStackedWidgetV1::abeStackedWidgetSetWrap(bool wrap)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    m_wrap=wrap;
}

void AbulEduStackedWidgetV1::abeStackedWidgetSlideInNext()
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    int now=currentIndex();
    if (m_wrap||(now<count()-1)) /* la fonction count() est héritée de QStackedWidget */
        abeStackedWidgetSlideInIndex(now+1);
}

void AbulEduStackedWidgetV1::abeStackedWidgetSlideInPrev()
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    int now=currentIndex();
    if (m_wrap||(now>0))
        abeStackedWidgetSlideInIndex(now-1);
}

void AbulEduStackedWidgetV1::abeStackedWidgetGoToNext()
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    int now=currentIndex();
    if (m_wrap||(now<count()-1)) /* la fonction count() est héritée de QStackedWidget */
        setCurrentIndex(now+1);
}

void AbulEduStackedWidgetV1::abeStackedWidgetGoToPrev()
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    int now=currentIndex();
    if (m_wrap||(now>0))
        setCurrentIndex(now-1);
}

void AbulEduStackedWidgetV1::abeStackedWidgetSlideInIndex(int idx, enum AbeStackedWidget_Direction direction)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    /* int idx, AbeStackedWidget_Direction direction=AUTOMATIC */
    if (idx>count()-1) {
        direction=m_vertical ? TOP2BOTTOM : RIGHT2LEFT;
        idx=(idx)%count();
    }
    else if (idx<0) {
        direction=m_vertical ? BOTTOM2TOP: LEFT2RIGHT;
        idx=(idx+count())%count();
    }
    abeStackedWidgetSlideInWidget(widget ( idx ),direction); /* widget() est une fonction héritée de QStackedWidget */
}

void AbulEduStackedWidgetV1::abeStackedWidgetSlideInWidget(QWidget * newwidget, enum AbeStackedWidget_Direction direction)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    if (m_active) {
        return;
        /* at the moment, do not allow re-entrance before an animation is completed.
         * other possibility may be to finish the previous animation abrupt, or
         * to revert the previous animation with a counter animation, before going ahead
         * or to revert the previous animation abrupt
         * and all those only, if the newwidget is not the same as that of the previous running animation.
         */
    }
    else m_active=true;

    enum AbeStackedWidget_Direction directionhint;
    int now=currentIndex();		/* currentIndex() est une fonction héritée de QStackedWidget */
    int next=indexOf(newwidget);
    if (now==next) {
        m_active=false;
        return;
    }
    else if (now<next){
        directionhint=m_vertical ? TOP2BOTTOM : RIGHT2LEFT;
    }
    else {
        directionhint=m_vertical ? BOTTOM2TOP : LEFT2RIGHT;
    }
    if (direction == AUTOMATIC) {
        direction=directionhint;
    }

    /* calculate the shifts */
    int offsetx=frameRect().width(); //inherited from mother
    int offsety=frameRect().height();//inherited from mother

    /* the following is important, to ensure that the new widget
     * has correct geometry information when sliding in first time */
    widget(next)->setGeometry ( 0,  0, offsetx, offsety );

    if (direction==BOTTOM2TOP)  {
        offsetx=0;
        offsety=-offsety;
    }
    else if (direction==TOP2BOTTOM) {
        offsetx=0;
        //offsety=offsety;
    }
    else if (direction==RIGHT2LEFT) {
        offsetx=-offsetx;
        offsety=0;
    }
    else if (direction==LEFT2RIGHT) {
        //offsetx=offsetx;
        offsety=0;
    }

    /* re-position the next widget outside/aside of the display area */
    QPoint pnext=widget(next)->pos();
    QPoint pnow=widget(now)->pos();
    m_pnow=pnow;

    widget(next)->move(pnext.x()-offsetx,pnext.y()-offsety);

    /* make it visible/show */
    widget(next)->show();
    widget(next)->raise();

    /* animate both, the now and next widget to the side, using animation framework */
    QPropertyAnimation *animnow = new QPropertyAnimation(widget(now), "pos");

    animnow->setDuration(m_speed);
    animnow->setEasingCurve(m_animationtype);
    animnow->setStartValue(QPoint(pnow.x(), pnow.y()));
    animnow->setEndValue(QPoint(offsetx+pnow.x(), offsety+pnow.y()));
    QPropertyAnimation *animnext = new QPropertyAnimation(widget(next), "pos");
    animnext->setDuration(m_speed);
    animnext->setEasingCurve(m_animationtype);
    animnext->setStartValue(QPoint(-offsetx+pnext.x(), offsety+pnext.y()));
    animnext->setEndValue(QPoint(pnext.x(), pnext.y()));

    QParallelAnimationGroup *animgroup = new QParallelAnimationGroup;

    animgroup->addAnimation(animnow);
    animgroup->addAnimation(animnext);

    QObject::connect(animgroup, SIGNAL(finished()),this,SLOT(slotAbeStackedWidgetAnimationDone()));
    m_next=next;
    m_now=now;
    m_active=true;
    animgroup->start();

    /* note; the rest is done via a connect from the animation ready;
     * animation->finished() provides a signal when animation is done;
     * so we connect this to some post processing slot,
     * that we implement here below in animationDoneSlot.
     */
}

void AbulEduStackedWidgetV1::slotAbeStackedWidgetAnimationDone(void)
{
    ABULEDU_LOG_TRACE() << __PRETTY_FUNCTION__;
    /* when ready, call the QStackedWidget slot setCurrentIndex(int) */
    setCurrentIndex(m_next);  /* Cette fonction est héritée de QStackedWidget */
    /* then hide the outshifted widget now, and  (may be done already implicitely by QStackedWidget) */
    widget(m_now)->hide();
    /* then set the position of the outshifted widget now back to its original */
    widget(m_now)->move(m_pnow);
    /* so that the application could also still call the QStackedWidget original functions/slots for changings */
    widget(m_now)->update();
    setCurrentIndex(m_next); /* Cette fonction est héritée de QStackedWidget */
    m_active=false;
    emit signalAbeStackedWidgetAnimationFinished();
}

