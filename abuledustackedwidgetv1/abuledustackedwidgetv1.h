/** @class AbulEduStackedWidgetV1
  * @file  abuledustackedwidgetv1.h
  *
  * @mainpage   AbulEduStackedWidgetV1
  *             Cette classe represente un QStackedWidget avec des effets d'animations type "slide". \n
  *             Cette animation peut-être horizontale ou verticale. \n
  *
  * @author 2013-2014 Philippe Cadaugade <philippe.cadaugade@ryxeo.com>
  * @author 2014 Icham Sirat <icham.sirat@ryxeo.com>
  * @date 2013 09 28
  * @see https://redmine.ryxeo.com/projects/ryxeo/wiki/AbulEduStackedWidgetV1
  * @see The GNU Public License (GPL)
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation; either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  * for more details.
  *
  * You should have received a copy of the GNU General Public License along
  * with this program; if not, write to the Free Software Foundation, Inc.,
  * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
  *
  * This file is part of The AbulEdu Project
  *
  */
#ifndef ABULEDUSTACKEDWIDGETV1_H
#define ABULEDUSTACKEDWIDGETV1_H

#include <QStackedWidget>
#include <QEasingCurve>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include "abuleduloggerv1.h"

class AbulEduStackedWidgetV1 : public QStackedWidget
{
    Q_OBJECT

public:
    //! This enumeration is used to define the animation direction
    enum AbeStackedWidget_Direction {
            LEFT2RIGHT,
            RIGHT2LEFT,
            TOP2BOTTOM,
            BOTTOM2TOP,
            AUTOMATIC
    };

    //! The Constructor and Destructor
    AbulEduStackedWidgetV1(QWidget *parent);
    ~AbulEduStackedWidgetV1(void);

public slots:
    /** Fixe la durée de la transition en millisecondes */
    void abeStackedWidgetSetSpeed(int speed);
    /** Fixe le type de QEasingCurve; initialisé à QEasingCurve::OutBack dans le constructeur d'AbulEduStackedWidgetV1 */
    void abeStackedWidgetSetAnimation(enum QEasingCurve::Type animationtype);
    /** Fixe l'orientation de l'animation; elle est initialisée à verticale dans le constructeur d'AbulEduStackedWidgetV1 */
    void abeStackedWidgetSetVerticalMode(bool vertical=true);
    /** Permet (à vérifier) de boucler sur l'animation */
    void abeStackedWidgetSetWrap(bool wrap);    //wrapping is related to slideInNext/Prev;it defines the behaviour when reaching last/first page

    /** Va à la page suivante avec effet */
    void abeStackedWidgetSlideInNext();
    /** Va à la page précédente avec effet */
    void abeStackedWidgetSlideInPrev();
    /** Va à la page suivante sans effet */
    void abeStackedWidgetGoToNext();
    /** Va à la page précédente sans effet */
    void abeStackedWidgetGoToPrev();
    /** Va à la page d'index idx */
    void abeStackedWidgetSlideInIndex(int idx, AbeStackedWidget_Direction direction=AUTOMATIC);
    /** Va à la page widget */
    void abeStackedWidgetSlideInWidget(QWidget * widget, enum AbeStackedWidget_Direction direction=AUTOMATIC);

signals:
    //! this is used for internal purposes in the class engine
    void signalAbeStackedWidgetAnimationFinished(void);

protected slots:
    //! this is used for internal purposes in the class engine
    void slotAbeStackedWidgetAnimationDone(void);

protected:

    QWidget *m_mainwindow;

    int m_speed;
    enum QEasingCurve::Type m_animationtype;
    bool m_vertical;
    int m_now;
    int m_next;
    bool m_wrap;
    QPoint m_pnow;
    bool m_active;

    QList<QWidget*> blockedPageList;
};

#endif // ABULEDUSTACKEDWIDGETV1_H
